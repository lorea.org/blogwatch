<?php
/**
 * blogwatch
 * 
 * @package Blogwatch
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
 * @author Alistair Young <alistair@codebrane.com>
 * @copyright codeBrane 2009
 * @link http://codebrane.com/blog/
 */

require_once($CONFIG->pluginspath."blogwatch/lib/blogwatchlib.php");

// elgg_echo doesn't work inline
$subscribe_button_text = elgg_echo("blogwatch:view:blogwatch:subscribe:button");
$unsubscribe_button_text = elgg_echo("blogwatch:view:blogwatch:unsubscribe:button");
$subscribers_button_text = elgg_echo("blogwatch:view:blogwatch:subscribers:button");
$subscribers_pop_title = elgg_echo("blogwatch:view:blogwatch:subscribers:popup:title");

if (isloggedin()) {
	$subscribed = "no";
	if (is_blog_subscriber($vars['entity']->getGUID(), $vars['user']->username)) {
		$subscribed = "yes";
	}

?>
<script type="text/javascript">
	function subscribe() {
		if (document.blogwatch_form.blogwatch_subscribe.checked) {
			window.alert("subscribe!");
		}
		else {
			window.alert("unsubscribe!");
		}
	}
</script>

	<form name="blogwatch_form" method="post" action="<? echo $vars['url'] . 'action/blogwatch/form' ?>">
		<?php if ($subscribed == "no") { ?>
			<input name="blogwatch_subscribe_button" type="submit" class="submit_button" value="<? echo $subscribe_button_text ?>" />
		<?php } else { ?>
			<input name="blogwatch_unsubscribe_button" type="submit" class="submit_button" value="<? echo $unsubscribe_button_text ?>" />
		<?php } ?>
		<?php if (blog_has_subscribers($vars['entity']->getGUID())) { ?>
			<input alt="<?php echo $vars['url'] . 'action/blogwatch/subscribers'; ?>?blog_guid=<?php echo $vars['entity']->getGUID() ?>&height=300&width=800" title="<?php echo $subscribers_pop_title ?> <?php echo $vars['entity']->title ?>" class="thickbox" type="button" value="<?php echo $subscribers_button_text ?>" />
		<?php } ?>
		<input type="hidden" name="blog_guid" value="<?php echo $vars['entity']->getGUID() ?>"/>
		<input type="hidden" name="blog_url" value="<?php echo $vars['entity']->getURL() ?>"/>
	</form>
<?php
} // if (isloggedin())
?>
